from flask.ext.wtf import Form
from wtforms import IntegerField, StringField, FloatField
from wtforms.validators import Required
from wtforms import ValidationError
from .models import Overdraft


class OverdraftForm(Form):
    name = StringField('Name', validators=[Required()])
    owed = IntegerField('Amount Owed', validators=[Required()])
    monthly_fee = IntegerField('Monthly Fee', validators=[Required()])

    def save(self):
        overdraft = Overdraft(name=self.name, owed=self.owed, monthly_fee=self.monthly_fee)
        return overdraft