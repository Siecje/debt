from flask import g, jsonify, make_response
from flask.ext.httpauth import HTTPBasicAuth
from app.accounts.models import User, AnonymousUser
from . import api
from .errors import unauthorized, forbidden

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(email_or_token, password):
    if email_or_token == '':
        g.current_user = AnonymousUser()
        return True
    if password == '':
        g.current_user = User.verify_auth_token(email_or_token)
        g.token_used = True
        return g.current_user is not None
    # TODO: only use email and password when getting a token?
    user = User.query.filter_by(email=email_or_token).first()
    if not user:
        return False
    g.current_user = user
    g.token_used = False
    return user.verify_password(password)


@auth.error_handler
def auth_error():
    return unauthorized('Invalid credentials')


@api.before_request
@auth.login_required
def before_request():
    # If it is an options request verify_password is skipped
    if 'current_user' in g:
        if not g.current_user.is_anonymous and \
                not g.current_user.confirmed:
            return forbidden('Unconfirmed account')


@api.after_request
def after_request(data):
    response = make_response(data)
    response.headers['Content-Type'] = 'Application/JSON'
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    return response


@api.route('/token', methods=['GET'])
def get_token():
    if g.current_user.is_anonymous() or g.token_used:
        return unauthorized('Invalid credentials.')
    # Create token valid for 24 hours
    return jsonify({'token': g.current_user.generate_auth_token(
        expiration=86400), 'expiration': 86400})


@api.route('/authenticate')
def authenticate():
    """ Verify the current token is valid """
    if g.current_user.is_anonymous():
        return unauthorized('Invalid credentials')
    return jsonify(g.current_user.to_json())
