from flask import jsonify, request, g, url_for
from app.core import db
from app.accounts.models import User
from . import api
from .errors import forbidden, bad_request
from .decorators import role_required


@api.route('/users/<int:id>')
@role_required('ADMIN_USER')
def get_user(id):
    user = User.query.get_or_404(id)
    return jsonify(user.to_json())


@api.route('/users/<email>')
@role_required('ADMIN_USER')
def get_user_with_email(email):
    user = User.query.filter_by(email=email).first_or_404()
    return jsonify(user.to_json())

@api.route('/users/all', methods=['GET'])
@role_required('ADMIN_USER')
def get_users():
    users = User.query.all()
    return jsonify({
        'users': [user.to_json() for user in users],
        'count': User.query.count()
    })


@api.route('/users/', methods=['POST'])
@role_required('ADMIN_USER')
def new_user():
    user = User.from_json(request.json)
    db.session.add(user)
    db.session.commit()
    return jsonify(user.to_json()), 201, \
        {'Location': url_for('api.get_user', id=user.id, _external=True)}


@api.route('/users/<int:id>')
@role_required('ADMIN_USER')
def edit_user(id):
    user = User.query.get_or_404(id)
    if g.current_user != user and g.current_user.can('CREATE_EDIT_ALL') is False:
        return forbidden('Insufficient permissions')
    user.name = request.json.get('name', user.name)
    user.location = request.json.get('location', user.location)
    user.about_me = request.json.get('about_me', user.about_me)

    db.session.add(user)
    return jsonify(user.to_json())


@api.route('/users/change-password', methods=['POST'])
def change_password():
    old_pass = request.json.get('old_password')
    new_pass = request.json.get('new_password')
    # Verify current password
    if g.current_user.verify_password(old_pass):
        # TODO: additional password restrictions?
        if new_pass != '':
            g.current_user.password = new_pass
            db.session.add(g.current_user)
            return jsonify({'response': 'Password has been changed.'})
        else:
            return bad_request('new_password is not valid.')
    return bad_request('old_password is not correct.')
