from flask.ext.wtf import Form
from wtforms import IntegerField, StringField, FloatField
from wtforms.validators import Required
from wtforms import ValidationError
from .models import Expense, Type


class ExpenseForm(Form):
    name = StringField('Name', validators=[Required()])
    amount = IntegerField('Amount', validators=[Required()])
    frequency = IntegerField('Frequency')
    type_id = IntegerField('Type')

    def save(self):
        expense = Expense(name=self.name, amount=self.amount)
        if self.frequency:
            expense.frequency = self.frequency
        if self.type_id:
            type = Type.query.get(self.type_id)
            if type:
                expense.type = type
        return expense