from flask import Response, json
from flask.testing import FlaskClient


_missing = object()


class TestResponse(Response):  # pragma: no cover

    @property
    def jdata(self):
        rv = getattr(self, '_cached_jdata', _missing)
        if rv is not _missing:
            return rv
        try:
            self._cached_jdata = json.loads(self.data)
        except ValueError:
            raise Exception('Invalid JSON response')
        return self._cached_jdata


class TestClient(FlaskClient):

    default_json_body_content_type = 'application/json'
    default_json_accept_content_type = 'application/json'

    def _default_json_kwargs(self, kwargs):
        kwargs.setdefault('headers', {})
        kwargs['headers'].setdefault('Accept', self.default_json_accept_content_type)
        kwargs['headers'].setdefault('Authorization', self.auth)
        kwargs.setdefault('content_type', self.default_json_body_content_type)
        if 'data' in kwargs:
            kwargs['data'] = json.dumps(kwargs['data'])
        return kwargs

    def jget(self, *args, **kwargs):
        return self.get(*args, **self._default_json_kwargs(kwargs))

    def jpost(self, *args, **kwargs):
        return self.post(*args, **self._default_json_kwargs(kwargs))

    def jput(self, *args, **kwargs):
        return self.put(*args, **self._default_json_kwargs(kwargs))

    def jpatch(self, *args, **kwargs):
        return self.patch(*args, **self._default_json_kwargs(kwargs))

    def jdelete(self, *args, **kwargs):
        return self.delete(*args, **self._default_json_kwargs(kwargs))
