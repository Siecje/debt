from flask.ext.wtf import Form
from wtforms import IntegerField, DateField, StringField
from wtforms.validators import Required
from wtforms import ValidationError
from .models import Income


class IncomeForm(Form):
    source = StringField(validators=[Required()])
    amount = IntegerField(validators=[Required()])
    frequency = IntegerField(validators=[Required()])
    date = DateField()

    def validate_amount(self, field):
        if field.data < 0:
            raise ValidationError('Amount cannot be negative.')

    def validate_frequency(self, field):
        if field.data < 0:
            raise ValidationError('Amount cannot be negative.')

    def save(self):
        income = Income(source=self.source.data, amount=self.amount.data, frequency=self.frequency.data)
        if self.date.data:
            income.date = self.date.data
        return income