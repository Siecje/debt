def print_form_errors(form):
    """ Must be called after validate """
    for field, errors in form.errors.items():
            for error in errors:
                print '%s - %s' % (getattr(form, field).label.text, error)

