from app.core import db


class Income(db.Model):
    __tablename__ = 'incomes'
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.Text)
    amount = db.Column(db.Integer)  # In cents
    frequency = db.Column(db.Integer)  # how often you receive self.amount
    date = db.Column(db.DateTime)  # one of the dates that you were paid

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref='incomes')

    def get_frequency_text(self):
        # TODO: convert to hourly?
        if self.frequency == 1:
            return 'hour'
        elif self.frequency == 2:
            return 'week'
        elif self.frequency == 3:
            return '2 weeks'
        elif self.frequency == 4:
            return 'month'

    def to_json(self):
        return {
            'source': self.source,
            'amount': self.amount,
            'frequency': self.frequency,
            'date': self.date,
            'user': {
                'name': self.report.user.name,
                'id': self.report.user.id
            }
        }

    def __unicode__(self):
        frequency_text = self.get_frequency_text()
        return '%s - $%s/%s' % (self.source, self.amount, frequency_text)