from functools import wraps
import json
import os
from flask import current_app, request
from jsonschema import Draft4Validator
from .errors import bad_request


def validate_json(*path):
    def wrapper(fn):
        @wraps(fn)
        def decorated(*args, **kwargs):
            # path[0] contains the file
            with open(os.path.join(current_app.config.get('JSON_SCHEMA_DIR'), path[0])) as f:
                schema = json.load(f)
            # subsequent parameters are the keys to follow in the JSON
            for lookup in path[1:]:
                schema = schema[lookup]
            v = Draft4Validator(schema)
            errors = {}
            for error in v.iter_errors(request.json):
                # Required field error
                if error.relative_schema_path[0] == 'required':
                    errors[error.message.split(' ')[0]] = 'Required property'
                elif error.relative_path:
                    errors[error.relative_path[0]] = error.message
                # Additional Field was found
                elif error.schema_path[0] == 'additionalProperties':
                    errors[error.message.split(' ')[5][:-1].replace("(u'", '')] = error.message

            if errors:
                return bad_request(errors)
            return fn(*args, **kwargs)
        return decorated
    return wrapper
