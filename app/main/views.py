from flask import render_template, abort, request, current_app
from flask.ext.sqlalchemy import get_debug_queries
from . import main


@main.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response


@main.route('/shutdown')
def server_shutdown():
    if not current_app.testing:
        abort(404)
    shutdown = request.environ.get('werkzeug.server.shutdown')
    if not shutdown:
        abort(500)
    shutdown()
    return 'Shutting down...'


@main.route('/', methods=['GET'])
def index():
    from app.debt.income.forms import IncomeForm
    from app.debt.credit_card.forms import CreditCardForm
    from app.debt.overdraft.forms import OverdraftForm
    return render_template('index.html',
        income_form=IncomeForm(),
        credit_card_form=CreditCardForm(),
        overdraft_form=OverdraftForm(),
    )
