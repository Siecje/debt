from flask import jsonify, request, url_for, g
from app.core import db
from app.debt.models import CreditCard
from . import api
from .decorators import role_required
from .errors import forbidden
from .json_validator import validate_json


@api.route('/credit-cards/', methods=['GET'])
@role_required('ADMIN_USER')
def get_all_credit_cards():
    credit_cards = CreditCard.query.all()
    return jsonify({
        'credit_cards': [credit_card.to_json() for credit_card in credit_cards],
        'count': len(credit_cards)
    })


@api.route('/credit-cards/<int:id>', methods=['GET'])
def get_credit_card(id):
    credit_card = CreditCard.query.get_or_404(id)
    if credit_card not in g.current_user.credit_cards and g.current_user.can('Administration') is False:
        return forbidden('Insufficient permissions.')
    return jsonify(credit_card.to_json())


@api.route('/credit-cards/', methods=['POST'])
@validate_json('credit_cards.json', 'create')
def new_credit_card():
    credit_card = CreditCard.from_json(request.json)
    credit_card.user = g.current_user
    db.session.add(credit_card)
    db.session.commit()
    return jsonify(credit_card.to_json()), 201, \
        {'Location': url_for('api.get_credit_card', id=credit_card.id, _external=True)}


@api.route('/credit-cards/<int:id>', methods=['PATCH'])
@validate_json('credit_cards.json', 'patch')
def edit_credit_card(id):
    credit_card = CreditCard.query.get_or_404(id)
    if credit_card not in g.current_user.credit_cards and g.current_user.can('Administration') is False:
        return forbidden('Insufficient permissions.')

    credit_card.name = request.json.get('name', credit_card.name)
    credit_card.interest_rate = request.json.get('interest_rate', credit_card.interest_rate)
    credit_card.owed = request.json.get('owed', credit_card.owed)
    credit_card.min_payment = request.json.get('min_payment', credit_card.min_payment)
    credit_card.min_payment_percent = request.json.get('min_payment_percent', credit_card.min_payment_percent)

    db.session.add(credit_card)
    return jsonify(credit_card.to_json())


@api.route('/credit-cards/<int:id>', methods=['PUT'])
@validate_json('credit_cards.json', 'put')
def replace_credit_card(id):
    credit_card = CreditCard.query.get_or_404(id)
    if credit_card not in g.current_user.credit_cards and g.current_user.can('Administration') is False:
        return forbidden('Insufficient permissions.')

    credit_card.name = request.json.get('name')
    credit_card.interest_rate = request.json.get('interest_rate')
    credit_card.owed = request.json.get('owed')
    credit_card.min_payment = request.json.get('min_payment')
    credit_card.min_payment_percent = request.json.get('min_payment_percent')

    db.session.add(credit_card)
    return jsonify(credit_card.to_json())


@api.route('/credit-cards/<int:id>', methods=['DELETE'])
# belongs to current user (or Admin) decorator
def delete_credit_card(id):
    credit_card = CreditCard.query.get_or_404(id)
    if credit_card not in g.current_user.credit_cards and g.current_user.can('Administration') is False:
        return forbidden('Failed to Delete Credit Card.')
    db.session.delete(credit_card)
    db.session.commit()
    response = jsonify({'message': 'Credit Card with id: %s deleted.' % id})
    response.status_code = 200
    return response
