from app.core import db


class Type(db.Model):
    __tablename__ = 'types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)

    # To show which types are available to each user
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref='types')

    def to_json(self):
        return {
            'name': self.name,
            'user': {
                'name': self.user.name,
                'id': self.user.id
            },
        }


class Expense(db.Model):
    __tablename__ = 'expenses'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    amount = db.Column(db.Integer)
    frequency = db.Column(db.Integer, default=0)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref='expenses')

    # One of self.user's defined types    
    type_id = db.Column(db.Integer, db.ForeignKey('types.id'))
    type = db.relationship('Type', backref='expenses')
    
    def to_json(self):
        return {
            'name': self.name,
            'amount': self.amount,
            'frequency': self.frequency,
            'user': {
                'name': self.user.name,
                'id': self.user.id
            },
            'type': {
                'name': self.type.name,
                'id': self.type.id
            }
        }
