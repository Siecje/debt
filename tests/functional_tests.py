from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def test_trying_it_out():
    browser = webdriver.Firefox()

    # Cameron has heard about a cool new online debt management app.
    # She goes to check out its homepage
    browser.get('http://localhost:5000')
    assert 'Debt' in browser.title

    # She notices the page invites her to get started right away without signup
    # TODO: test for text

    # She decides to try the service

    # She is invited to enter a income or debt amount straight away
    assert 'Add Income Source' in browser.find_element_by_id('income').text
    assert 'Add Credit Card' in browser.find_element_by_id('credit-card').text
    assert 'Overdraft' in browser.find_element_by_id('overdraft').text

    # She enters her income per hour and job title
    income_amount = browser.find_element_by_id('income-amount')
    income_amount.send_keys('15')

    income_name = browser.find_element_by_id('income-name')
    income_name.send_keys('Boilermaker')

    # When she hits enter, the page changes and lists an income item
    # "Job Title" - amount
    income_name.send_keys(Keys.ENTER)
    assert 'Boilermaker - $15/hour' in browser.find_element_by_id('income').text

    # She enters a creditcard balance, with the card name
    credit_card_amount_owned = browser.find_element_by_id('credit-card-balance')
    credit_card_amount_owned.send_keys('500')

    credit_card_name = browser.find_element_by_id('credit-card-name')
    credit_card_name.send_keys('Visa')

    credit_card_interest = browser.find_element_by_id('credit-card-interest')
    credit_card_interest.send_keys('19.99')
    credit_card_interest.send_keys(Keys.ENTER)

    # When she hits enter, the page lists her credit card name and balance
    # "Card Name" - balance
    credit_cards = browser.find_element_by_id('credit-card')
    assert 'Visa' in browser.find_element_by_id('credit-card').text
    assert '$500' in browser.find_element_by_id('credit-card').text

    # TODO: use
    # assert '19.99%' in browser.find_element_by_id('credit-card').text
    assert '19.99' in browser.find_element_by_id('credit-card').text

    # The page updates and shows a graph
    # TODO: how to test graph

    browser.quit()


def test_all_objects():
    browser = webdriver.Firefox()

    # Cameron has heard about a cool new online debt management app.
    # She goes to check out its homepage
    browser.get('http://localhost:5000')
    browser.quit()


if __name__ == '__main__':
    test_trying_it_out()
