from base64 import b64encode
import pytest
from app import create_app
from app.core import db
from app.accounts.models import User, Role
from test_client import TestClient, TestResponse
from sqlalchemy.orm import sessionmaker, scoped_session


@pytest.fixture(scope='session')
def app():
    app = create_app('testing')
    app.config['SERVER_NAME'] = 'example.com:1234'
    app.response_class = TestResponse
    app.test_client_class = TestClient

    ctx = app.app_context()
    ctx.push()

    return app


@pytest.fixture(scope='session')
def connection(app, request):
    connection = db.engine.connect()
    transaction = connection.begin()

    db.metadata.create_all(connection)

    session_factory = sessionmaker(bind=connection)
    db.session = scoped_session(session_factory)

    Role.insert_initial()

    user = User(email='admin@example.com', username='admin', confirmed=True, password='admin', name='Admin')
    user.role = db.session.query(Role).filter_by(name='Administrator').first()
    db.session.add(user)
    db.session.commit()

    # After all tests restore database to empty tables
    request.addfinalizer(transaction.rollback)
    return connection


@pytest.fixture(scope='function')
def db_session(connection, request):
    ''' Create a new database session for each test. '''
    transaction = connection.begin_nested()
    session_factory = sessionmaker(bind=connection)
    db.session = scoped_session(session_factory)

    # revert all commits in each test
    request.addfinalizer(transaction.rollback)
    return db.session


@pytest.fixture(scope='function')
def user(db_session):
    return db_session.query(User).filter_by(email='admin@example.com').first()


@pytest.fixture(scope='function')
def client(app):
    client = app.test_client()
    client.auth = 'Basic ' + b64encode(
        ('admin@example.com' + ':' + 'admin').encode('utf-8')).decode('utf-8')
    return client
