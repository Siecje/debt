Debt
======
```bash
set -x DEV_DATABASE_URL "postgres://debt_user:debt_user@localhost/debt"
set -x TEST_DATABASE_URL "postgres://test_debt_user:test_debt_user@localhost/test_debt"

virtualenv venv
source venv/bin/activate.fish 
pip install -r requirements/dev.txt
python manage.py db init
python manage.py db migrate -m "initial"
python manage.py deploy

```
