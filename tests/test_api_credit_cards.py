from app.core import db
from app.debt.models import CreditCard


def test_create_credit_card(db_session, client):
    response = client.jpost('/api/v1.0/credit-cards/', data={
        'name': 'From API',
        'owed': 1000,
        'interest_rate': 0.18,
        'min_payment': 10,
        'min_payment_percent': 0.03
    })
    assert response.status_code == 201
    response = client.jget('/api/v1.0/credit-cards/%s' % response.jdata['id'])
    assert response.status_code == 200

def test_create_credit_card_without_name(db_session, client):
    response = client.jpost('/api/v1.0/credit-cards/', data={
        'owed': 1000,
        'interest_rate': 0.18,
        'min_payment': 10,
        'min_payment_percent': 0.03
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {"u'name'": "Required property"}

def test_create_credit_card_with_no_name_and_invalid_interest_rate(db_session, client):
    response = client.jpost('/api/v1.0/credit-cards/', data={
        'owed': 1000,
        'interest_rate': 'invalid',
        'min_payment': 10,
        'min_payment_percent': 0.03
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {u"u'name'": u'Required property',
                                         u'interest_rate': u"u'invalid' is not of type u'number'"}

def test_create_credit_card_with_no_name_and_invlid_interest_rate_and_extra_field(db_session, client):
    response = client.jpost('/api/v1.0/credit-cards/', data={
        'owed': 1000,
        'interest_rate': 'invalid',
        'min_payment': 10,
        'min_payment_percent': 0.03,
        'extra': 1
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {u'interest_rate': u"u'invalid' is not of type u'number'",
                                         u'extra': u"Additional properties are not allowed (u'extra' was unexpected)",
                                         u"u'name'": u'Required property'}


def test_edit_credit_card(db_session, client, user):
    credit_card = CreditCard(name='First', owed=1000, interest_rate=0.18,
                             min_payment=10, min_payment_percent=0.03, user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jpatch('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'name': 'second',
    })
    assert response.status_code == 200
    assert response.jdata['name'] == 'second'

def test_edit_credit_card_with_bad_interest_rate(db_session, client, user):
    credit_card = CreditCard(name='First', owed=1000, interest_rate=0.18,
                             min_payment=10, min_payment_percent=0.03, user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jpatch('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'interest_rate': 'invalid',
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {'interest_rate': "u'invalid' is not of type u'number'"}

def test_edit_credit_card_with_extra_field(db_session, client, user):
    credit_card = CreditCard(name='Validate', user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jpatch('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'dne': 'invalid',
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {'dne': "Additional properties are not allowed (u'dne' was unexpected)"}

def test_edit_credit_card_with_multiple_errors(db_session, client, user):
    credit_card = CreditCard(name='Validate', user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jpatch('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'interest_rate': 'invalid',
        'owed': 'invalid',
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {'interest_rate': "u'invalid' is not of type u'number'",
                                         'owed': "u'invalid' is not of type u'integer'"}

def test_edit_credit_card_with_invalid_credit_card_name(db_session, client, user):
    credit_card = CreditCard(name='Validate', user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jpatch('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'name': ['invalid', 2],
    })
    assert response.status_code == 400
    assert response.jdata['message'] == {'name': "[u'invalid', 2] is not of type u'string'"}


def test_replace_credit_card(db_session, client, user):
    credit_card = CreditCard(name='No Delivery Methods', user=user)
    db.session.add(credit_card)
    db.session.commit()

    response = client.jput('/api/v1.0/credit-cards/%s' % credit_card.id, data={
        'name': 'Proper Name',
        'owed': 1000,
        'interest_rate': 0.18,
        'min_payment': 10,
        'min_payment_percent': 0.03,
    })

    assert response.status_code == 200


def test_delete_credit_card(db_session, client, user):
    credit_card = CreditCard(name='No Delivery Methods')
    credit_card.user = user
    db.session.add(credit_card)
    db.session.commit()

    response = client.jdelete('/api/v1.0/credit-cards/%s' % credit_card.id)
    assert response.status_code == 200
    assert response.jdata['message'] == 'Credit Card with id: %d deleted.' % credit_card.id
    assert db.session.query(CreditCard).get(credit_card.id) is None
