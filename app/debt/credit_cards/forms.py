from flask.ext.wtf import Form
from wtforms import IntegerField, StringField, FloatField
from wtforms.validators import Required
from wtforms import ValidationError
from .models import CreditCard


class CreditCardForm(Form):
    name = StringField('Name', validators=[Required()])
    interest_rate = FloatField('Interest Rate', validators=[Required()])
    owed = IntegerField('Amount Owed', validators=[Required()])
    min_payment = IntegerField('Minimum Payment Amount')
    min_payment_percent = FloatField('Minimum Payment Percentage')
    annual_fee = IntegerField('Annual Fee')

    def validate_interest_rate(self, field):
        if field.data < 0:
            print field.data
            raise ValidationError('Amount cannot be negative.')

    def validate_owed(self, field):
        if field.data < 0:
            print field.data
            raise ValidationError('Amount cannot be negative.')

    def validate_min_payment(self, field):
        if field.data is not None and field.data < 0:
            raise ValidationError('Amount cannot be negative.')

    def validate_min_payment_percent(self, field):
        if field.data is not None and field.data < 0:
            raise ValidationError('Amount cannot be negative.')

    def validate_annual_fee(self, field):
        if field.data is not None and field.data < 0:
            raise ValidationError('Amount cannot be negative.')

    #TODO: validate at least one of min_payment or min_payment_percent

    def save(self):
        credit_card = CreditCard(name=self.name.data, interest_rate=self.interest_rate.data, owed=self.owed.data)
        if self.min_payment is None and self.min_payment_percent is None:
            return None
        if self.min_payment:
            credit_card.min_payment = self.min_payment.data
        if self.min_payment_percent:
            credit_card.min_payment_percent = self.min_payment_percent.data
        if self.annual_fee:
            credit_card.min_payment = self.annual_fee.data
        return credit_card