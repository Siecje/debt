from app.core import db


class CreditCard(db.Model):
    __tabelname__ = 'credit_cards'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    # TODO: nullable=False or auto save as you complete?
    interest_rate = db.Column(db.Float)
    owed = db.Column(db.Integer)
    min_payment = db.Column(db.Integer)
    min_payment_percent = db.Column(db.Float)
    annual_fee = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('User', backref='credit_cards')

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'interest_rate': self.interest_rate,
            'owed': self.owed,
            'min_payment': self.min_payment,
            'min_payment_percent': self.min_payment_percent,
            'annual_fee': self.annual_fee,
            'user': {
                'name': self.user.name,
                'id': self.user.id
            }
        }

    @staticmethod
    def from_json(credit_card_json):
        return CreditCard(name=credit_card_json.get('name'),
                          owed=credit_card_json.get('owed'),
                          interest_rate=credit_card_json.get('interest_rate'),
                          min_payment=credit_card_json.get('min_payment'),
                          min_payment_percent=credit_card_json.get('min_payment_percent'),
                          annual_fee=credit_card_json.get('annual_fee')
        )


    def __unicode__(self):
        # TODO: how to put % here
        return '%s (%s): $%s' % (self.name, self.interest_rate, self.owed)