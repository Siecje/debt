from werkzeug.datastructures import MultiDict
from app.core import db
from app.debt.models import Report


def test_home_page_exists(app):
    with app.test_client() as client:
        response = client.get('/')
    
    assert response.status_code == 200


def print_form_errors(form):
    """ Must be called after validate """
    for field, errors in form.errors.items():
            for error in errors:
                print '%s - %s' % (getattr(form, field).label.text, error)


def test_income_form(app):
    with app.test_request_context() as context:
        from app.debt.income.forms import IncomeForm
        form = IncomeForm()
        assert form.validate() is False
        form = IncomeForm(MultiDict({'source': 'Test', 'frequency': 5, 'amount': 12}))
        form.validate()
        print_form_errors(form)
        assert form.validate()
        income = form.save()
        assert income is not None


def test_create_report_with_income(app):
    with app.test_client() as client:
        response = client.post('/create-report', data={
            'source': 'Test',
            'frequency': 5,
            'amount': 12
        })
        assert response.status_code == 302
        assert response.headers['Location'].startswith('http://localhost/report/')


def test_create_credit_card(app):
    report = Report()
    db.session.add(report)
    db.session.commit()
    with app.test_client() as client:
        response = client.post('/report/%s/create-credit-card' % report.id, data={
            'name': 'Master',
            'amount': 500,
            'interest': 19.99
        })
        assert response.status_code == 302
        assert response.headers['Location'] == 'http://localhost/report/%s' % report.id
