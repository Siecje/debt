from app.core import db


class Overdraft(db.Model):
    __tabelname__ = 'overdrafts'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    owed = db.Column(db.Integer)
    monthly_fee = db.Column(db.Integer)
    
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref='overdrafts')

    def to_json(self):
        return {
            'name': self.name,
            'owed': self.owed,
            'monthly_fee': self.monthly_fee,
            'user': {
                'name': self.report.user.name,
                'id': self.report.user.id
            }
        }
